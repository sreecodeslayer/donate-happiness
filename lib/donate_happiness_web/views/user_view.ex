defmodule DonateHappinessWeb.UserView do
  use DonateHappinessWeb, :view
  alias DonateHappinessWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      dob: user.dob,
      inserted_at: user.inserted_at
    }
  end

  def render("jwt.json", %{jwt: jwt, user: user}) do
    %{token: jwt, user: render_one(user, UserView, "user.json")}
  end

  def render("jwt.json", %{jwt: jwt}) do
    %{token: jwt}
  end
end
