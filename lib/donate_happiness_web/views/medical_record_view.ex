defmodule DonateHappinessWeb.MedicalRecordView do
  use DonateHappinessWeb, :view
  alias DonateHappinessWeb.MedicalRecordView

  def render("index.json", %{medical_records: medical_records}) do
    %{data: render_many(medical_records, MedicalRecordView, "medical_record.json")}
  end

  def render("show.json", %{medical_record: medical_record}) do
    %{data: render_one(medical_record, MedicalRecordView, "medical_record.json")}
  end

  def render("medical_record.json", %{medical_record: medical_record}) do
    %{id: medical_record.id,
      blood_group: medical_record.blood_group,
      differently_abled: medical_record.differently_abled}
  end
end
