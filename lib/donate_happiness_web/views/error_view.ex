defmodule DonateHappinessWeb.ErrorView do
  use DonateHappinessWeb, :view

  # If you want to customize a particular status code
  # for a certain format, you may uncomment below.
  # def render("500.json", _assigns) do
  #   %{errors: %{detail: "Internal Server Error"}}
  # end

  # By default, Phoenix returns the status message from
  # the template name. For example, "404.json" becomes
  # "Not Found".
  def template_not_found(template, _assigns) do
    %{errors: %{detail: Phoenix.Controller.status_message_from_template(template)}}
  end

  def render("404.json", %{reason: reason}) do
    %{
      error: [reason],
      code: 404,
      message: "Not Found"
    }
  end

  def render("401.json", %{reason: reason}) do
    %{
      error: [reason],
      code: 401,
      message: "Unauthorized"
    }
  end

  def render("403.json", %{reason: reason}) do
    %{
      error: [reason],
      code: 403,
      message: "Forbidden"
    }
  end
end
