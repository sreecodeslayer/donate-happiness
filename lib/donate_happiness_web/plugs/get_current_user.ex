defmodule DonateHappinessWeb.GetCurrentUserPlug do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    if conn.assigns[:current_user] do
      conn
    else
      put_current_user(conn)
    end
  end

  def put_current_user(conn) do
    user = Guardian.Plug.current_resource(conn)

    conn
    |> assign(:current_user, user)
  end
end
