defmodule DonateHappinessWeb.UserController do
  use DonateHappinessWeb, :controller

  alias DonateHappiness.Accounts

  alias DonateHappiness.Accounts.{
    User,
    Auth
  }

  action_fallback DonateHappinessWeb.FallbackController

  def login(conn, %{"user" => login_params}) do
    case Auth.check_user_creds(login_params["email"], login_params["password"]) do
      {:ok, user, token} ->
        conn
        |> put_status(:accepted)
        |> render("jwt.json", jwt: token, user: user)

      {:error, reason} ->
        conn
        |> put_status(:not_found)
        |> put_view(DonateHappinessWeb.ErrorView)
        |> render("404.json", %{reason: reason})
    end
  end

  def signup(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, %User{} = user} ->
        {:ok, token, _claims} = Auth.create_token(user)

        conn
        |> put_status(:created)
        |> render("jwt.json", jwt: token, user: user)

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> put_view(DonateHappinessWeb.ChangesetView)
        |> render("error.json", changeset: changeset)
    end
  end
end
