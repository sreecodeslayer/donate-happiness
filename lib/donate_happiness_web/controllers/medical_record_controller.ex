defmodule DonateHappinessWeb.MedicalRecordController do
  use DonateHappinessWeb, :controller

  alias DonateHappiness.Medical
  alias DonateHappiness.Medical.MedicalRecord

  action_fallback DonateHappinessWeb.FallbackController

  def index(conn, _params) do
    medical_records = Medical.list_medical_records()
    render(conn, "index.json", medical_records: medical_records)
  end

  def create(conn, %{"medical_record" => medical_record_params}) do
    with {:ok, %MedicalRecord{} = medical_record} <-
           Medical.create_medical_record(medical_record_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.medical_record_path(conn, :show, medical_record))
      |> render("show.json", medical_record: medical_record)
    end
  end

  def show(conn, %{"id" => id}) do
    medical_record = Medical.get_medical_record!(id)
    render(conn, "show.json", medical_record: medical_record)
  end

  def update(conn, %{"id" => id, "medical_record" => medical_record_params}) do
    medical_record = Medical.get_medical_record!(id)

    with {:ok, %MedicalRecord{} = medical_record} <-
           Medical.update_medical_record(medical_record, medical_record_params) do
      render(conn, "show.json", medical_record: medical_record)
    end
  end

  def delete(conn, %{"id" => id}) do
    medical_record = Medical.get_medical_record!(id)

    with {:ok, %MedicalRecord{}} <- Medical.delete_medical_record(medical_record) do
      send_resp(conn, :no_content, "")
    end
  end
end
