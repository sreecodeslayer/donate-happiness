defmodule DonateHappinessWeb.Router do
  use DonateHappinessWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :ensure_header do
    plug(DonateHappiness.Guardian.GuardianPipeline)
  end

  # We use ensure_auth to fail if there is no one logged in
  pipeline :ensure_auth do
    plug Guardian.Plug.EnsureAuthenticated,
      claims: %{"typ" => "access"},
      error_handler: DonateHappiness.Guardian.GuardianErrorHandler
  end

  pipeline :with_current_user do
    plug DonateHappinessWeb.GetCurrentUserPlug,
      error_handler: DonateHappiness.Guardian.GuardianErrorHandler
  end

  scope "/", DonateHappinessWeb do
    pipe_through :api

    post "/users/login", UserController, :login
    post "/users/signup", UserController, :signup
  end

  scope "/api", DonateHappinessWeb do
    pipe_through([:api, :ensure_header, :ensure_auth, :with_current_user])

    resources "/medical-records", MedicalRecordController
  end
end
