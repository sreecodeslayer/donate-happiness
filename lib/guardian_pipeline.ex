defmodule DonateHappiness.Guardian.GuardianPipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :donate_happiness,
    error_handler: DonateHappiness.Guardian.GuardianErrorHandler,
    module: DonateHappiness.Guardian

  # If there is an authorization header, restrict it to an access token and validate it
  plug Guardian.Plug.VerifyHeader, realm: "Bearer", claims: %{"typ" => "access"}
  # Load the user if either of the verifications worked
  plug Guardian.Plug.LoadResource, allow_blank: false
end
