defmodule DonateHappiness.Guardian do
  use Guardian, otp_app: :donate_happiness
  alias DonateHappiness.Accounts.User
  alias DonateHappiness.Accounts

  def subject_for_token(%User{} = resource, _claims) do
    sub = to_string(resource.id)
    {:ok, sub}
  end

  def subject_for_token(resource, _) do
    {:error, "Found resource as #{inspect(resource)}"}
  end

  def resource_from_claims(claims) when is_map(claims) do
    id = claims["sub"]
    resource = Accounts.get_user!(id)
    {:ok, resource}
  end

  def resource_from_claims(_claims) do
    {:error, "Claim is expected to be a Map"}
  end
end
