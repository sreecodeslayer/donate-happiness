defmodule DonateHappiness.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias DonateHappiness.Accounts.{
    User,
    Auth
  }

  @email_regex ~r(^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$)
  @required_fields ~w(fullname dob email password)a
  @cast_fields ~w(fullname email password dob reset_password_token reset_password_sent_at)a

  schema "users" do
    field :fullname, :string
    field :email, :string
    field :password, :string, virtual: true
    field :hashed_password, :string
    field :reset_password_sent_at, :utc_datetime
    field :reset_password_token, :string
    field :dob, :date

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, @cast_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, @email_regex)
    |> unique_constraint(:email)
    |> validate_length(:password, min: 8)
    |> put_hashed_password()
  end

  @doc false
  def reset_password_changeset(%User{} = user, attrs) do
    user |> cast(attrs, [:reset_password_token, :reset_password_sent_at])
  end

  defp put_hashed_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :hashed_password, Auth.generate_hashed_password(password))

      _ ->
        changeset
    end
  end
end
