defmodule DonateHappiness.Accounts.Auth do
  defmodule UnauthorizedError do
    @moduledoc """
    Exception raised when an unauthorized request is made for a given resource
    """
    defexception plug_status: 401, message: "Unauthorized", reason: ""

    def exception(reason) do
      %UnauthorizedError{reason: reason}
    end
  end

  use Ecto.Schema
  alias DonateHappiness.Accounts.User
  alias DonateHappiness.Accounts
  alias DonateHappiness.Guardian

  def check_user_creds(email, password) do
    case Accounts.get_user_by(email: email) do
      nil ->
        Bcrypt.no_user_verify()
        {:error, "Login error."}

      user ->
        verify_password(password, user)
    end
  end

  def create_token(%User{} = user) do
    Guardian.encode_and_sign(user)
  end

  def generate_hashed_password(raw_password) do
    Bcrypt.hash_pwd_salt(raw_password)
  end

  defp verify_password(password, %User{} = user) when is_binary(password) do
    case Bcrypt.check_pass(user, password, hash_key: :hashed_password) do
      {:ok, user} ->
        {:ok, token, _others} = create_token(user)
        {:ok, user, token}

      {:error, "invalid password"} ->
        {:error, "The credentials seems invalid. Please recheck the email and password"}
    end
  end
end
