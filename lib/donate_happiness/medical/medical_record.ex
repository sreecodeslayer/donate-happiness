defmodule DonateHappiness.Medical.MedicalRecord do
  use Ecto.Schema
  import Ecto.Changeset
  alias DonateHappiness.Accounts.User

  @valid_blood_groups ~W(A+ B+ A- B- O+ O- AB- AB+)
  @required_fields ~W(blood_group differently_abled)a

  schema "medical_records" do
    field :blood_group, :string
    field :differently_abled, :boolean, default: false
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(medical_record, attrs) do
    medical_record
    |> cast(attrs, [:blood_group, :differently_abled])
    |> cast_assoc(:user)
    |> validate_required(@required_fields)
    |> validate_inclusion(:blood_group, @valid_blood_groups)
  end
end
