defmodule DonateHappiness.Repo do
  use Ecto.Repo,
    otp_app: :donate_happiness,
    adapter: Ecto.Adapters.Postgres
end
