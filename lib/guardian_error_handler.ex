defmodule DonateHappiness.Guardian.GuardianErrorHandler do
  import Plug.Conn
  use DonateHappinessWeb, :controller

  @behaviour Guardian.Plug.ErrorHandler

  @impl Guardian.Plug.ErrorHandler
  def auth_error(conn, {:no_resource_found, _reason}, _opts) do
    conn
    |> put_status(:unauthorized)
    |> put_view(DonateHappinessWeb.ErrorView)
    |> render("401.json", %{reason: "Authorization with a Bearer token is missing."})
  end

  @impl Guardian.Plug.ErrorHandler
  def auth_error(conn, {:invalid_token, _reason}, _opts) do
    conn
    |> put_status(:unauthorized)
    |> put_view(DonateHappinessWeb.ErrorView)
    |> render("401.json", %{reason: "Invalid token provided."})
  end

  @impl Guardian.Plug.ErrorHandler
  def auth_error(conn, {:unauthenticated, _reason}, _opts) do
    conn
    |> put_status(:unauthorized)
    |> put_view(DonateHappinessWeb.ErrorView)
    |> render("401.json", %{reason: "You are not authorized. Please login"})
  end
end
