defmodule DonateHappiness.MedicalTest do
  use DonateHappiness.DataCase

  alias DonateHappiness.Medical

  describe "medical_records" do
    alias DonateHappiness.Medical.MedicalRecord

    @valid_attrs %{blood_group: "A+", differently_abled: true}
    @update_attrs %{blood_group: "B+", differently_abled: false}
    @invalid_attrs %{blood_group: nil, differently_abled: nil}

    def medical_record_fixture(attrs \\ %{}) do
      {:ok, medical_record} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Medical.create_medical_record()

      medical_record
    end

    test "list_medical_records/0 returns all medical_records" do
      medical_record = medical_record_fixture()
      assert Medical.list_medical_records() == [medical_record]
    end

    test "get_medical_record!/1 returns the medical_record with given id" do
      medical_record = medical_record_fixture()
      assert Medical.get_medical_record!(medical_record.id) == medical_record
    end

    test "create_medical_record/1 with valid data creates a medical_record" do
      assert {:ok, %MedicalRecord{} = medical_record} =
               Medical.create_medical_record(@valid_attrs)

      assert medical_record.blood_group == "A+"
      assert medical_record.differently_abled == true
    end

    test "create_medical_record/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Medical.create_medical_record(@invalid_attrs)
    end

    test "update_medical_record/2 with valid data updates the medical_record" do
      medical_record = medical_record_fixture()

      assert {:ok, %MedicalRecord{} = medical_record} =
               Medical.update_medical_record(medical_record, @update_attrs)

      assert medical_record.blood_group == "B+"
      assert medical_record.differently_abled == false
    end

    test "update_medical_record/2 with invalid data returns error changeset" do
      medical_record = medical_record_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Medical.update_medical_record(medical_record, @invalid_attrs)

      assert medical_record == Medical.get_medical_record!(medical_record.id)
    end

    test "delete_medical_record/1 deletes the medical_record" do
      medical_record = medical_record_fixture()
      assert {:ok, %MedicalRecord{}} = Medical.delete_medical_record(medical_record)
      assert_raise Ecto.NoResultsError, fn -> Medical.get_medical_record!(medical_record.id) end
    end

    test "change_medical_record/1 returns a medical_record changeset" do
      medical_record = medical_record_fixture()
      assert %Ecto.Changeset{} = Medical.change_medical_record(medical_record)
    end
  end
end
