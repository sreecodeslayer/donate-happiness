defmodule DonateHappiness.AccountsTest do
  use DonateHappiness.DataCase

  alias DonateHappiness.Accounts

  describe "users" do
    alias DonateHappiness.Accounts.User

    @valid_attrs %{
      dob: "2010-04-17",
      email: "someone@email.com",
      fullname: "some fullname",
      password: "some password",
      reset_password_sent_at: "2010-04-17T14:00:00Z",
      reset_password_token: "some reset_password_token"
    }
    @update_attrs %{
      dob: "2011-05-18",
      email: "someonenew@email.com",
      fullname: "some updated fullname",
      password: "updated password",
      reset_password_sent_at: "2011-05-18T15:01:01Z",
      reset_password_token: "some updated reset_password_token"
    }
    @invalid_attrs %{
      dob: nil,
      email: nil,
      fullname: nil,
      password: nil,
      reset_password_sent_at: nil,
      reset_password_token: nil
    }

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      [first_user] = Accounts.list_users()
      assert first_user.id == user.id
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      fetched_user = Accounts.get_user!(user.id)
      assert fetched_user.id == user.id
      assert fetched_user.email == user.email
      assert fetched_user.fullname == user.fullname
      assert fetched_user.hashed_password == user.hashed_password
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.dob == ~D[2010-04-17]
      assert user.email == @valid_attrs.email
      assert user.fullname == @valid_attrs.fullname

      assert user.reset_password_sent_at ==
               DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")

      assert user.reset_password_token == "some reset_password_token"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      olduser = user_fixture()
      assert {:ok, %User{} = user} = Accounts.update_user(olduser, @update_attrs)
      assert user.dob == ~D[2011-05-18]
      assert user.email == @update_attrs.email
      assert user.hashed_password != olduser.hashed_password

      assert user.reset_password_sent_at == 
               DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert user.reset_password_token == "some updated reset_password_token"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Accounts.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
