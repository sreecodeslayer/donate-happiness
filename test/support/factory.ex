defmodule DonateHappiness.Factory do
  alias Plug.Conn
  alias DonateHappiness.Repo
  alias DonateHappiness.Accounts
  alias DonateHappiness.Accounts.Auth

  def insert(model, attrs \\ %{}, data \\ %{})

  def insert(:user, attrs, _data) do
    name = "user#{System.unique_integer([:positive])}"

    {:ok, user} =
      attrs
      |> Enum.into(%{
        fullname: name,
        email: attrs[:email] || "#{name}@test.com",
        password: attrs[:password] || "supersecret",
        dob: attrs[:dob] || "1990-11-28"
      })
      |> stringify()
      |> Accounts.create_user()

    user
  end

  def login_and_add_to_conn(conn) do
    user = insert(:user, %{password: "test-password"})

    # Mock login and add token in header
    {:ok, _user, token} = Auth.check_user_creds(user.email, "test-password")

    conn =
      conn
      |> Plug.Test.init_test_session(%{current_user: user.id})
      |> Conn.put_req_header("accept", "application/json")
      |> Conn.put_req_header("authorization", "Bearer #{token}")

    {:ok, conn: conn}
  end

  defp stringify(attrs) do
    for {key, val} <- attrs, into: %{} do
      cond do
        is_binary(key) -> {key, val}
        true -> {Atom.to_string(key), val}
      end
    end
  end
end
