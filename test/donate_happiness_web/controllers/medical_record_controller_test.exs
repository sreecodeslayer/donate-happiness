defmodule DonateHappinessWeb.MedicalRecordControllerTest do
  use DonateHappinessWeb.ConnCase

  alias DonateHappiness.Medical
  alias DonateHappiness.Medical.MedicalRecord

  @create_attrs %{
    blood_group: "A+",
    differently_abled: true
  }
  @update_attrs %{
    blood_group: "B+",
    differently_abled: false
  }
  @invalid_attrs %{blood_group: nil, differently_abled: nil}

  def fixture(:medical_record) do
    {:ok, medical_record} = Medical.create_medical_record(@create_attrs)
    medical_record
  end

  setup %{conn: conn} do
    login_and_add_to_conn(conn)
  end

  describe "index" do
    test "lists all medical_records", %{conn: conn} do
      conn = get(conn, Routes.medical_record_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create medical_record" do
    test "renders medical_record when data is valid", %{conn: conn} do
      conn = post(conn, Routes.medical_record_path(conn, :create), medical_record: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.medical_record_path(conn, :show, id))

      assert %{
               "id" => id,
               "blood_group" => "A+",
               "differently_abled" => true
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.medical_record_path(conn, :create), medical_record: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update medical_record" do
    setup [:create_medical_record]

    test "renders medical_record when data is valid", %{
      conn: conn,
      medical_record: %MedicalRecord{id: id} = medical_record
    } do
      conn =
        put(conn, Routes.medical_record_path(conn, :update, medical_record),
          medical_record: @update_attrs
        )

      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.medical_record_path(conn, :show, id))

      assert %{
               "id" => id,
               "blood_group" => "B+",
               "differently_abled" => false
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, medical_record: medical_record} do
      conn =
        put(conn, Routes.medical_record_path(conn, :update, medical_record),
          medical_record: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete medical_record" do
    setup [:create_medical_record]

    test "deletes chosen medical_record", %{conn: conn, medical_record: medical_record} do
      conn = delete(conn, Routes.medical_record_path(conn, :delete, medical_record))
      assert response(conn, 204)

      conn = get(conn, Routes.medical_record_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  defp create_medical_record(_) do
    medical_record = fixture(:medical_record)
    {:ok, medical_record: medical_record}
  end
end
