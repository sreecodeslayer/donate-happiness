defmodule DonateHappinessWeb.UserControllerTest do
  use DonateHappinessWeb.ConnCase

  alias DonateHappiness.Accounts

  @create_attrs %{
    dob: "2010-04-17",
    email: "someone@email.com",
    password: "some password",
    fullname: "some fullname"
  }
  @invalid_attrs %{
    dob: nil,
    email: nil,
    fullname: nil,
    password: nil
  }

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_attrs)
    user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "signup user" do
    test "renders user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :signup), user: @create_attrs)

      assert %{
        "user" => %{
               "id" => _id,
               "dob" => "2010-04-17",
               "email" => "someone@email.com",
               "fullname" => "some fullname"
        },
        "token" => _token} = json_response(conn, 201)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :signup), user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end
end
