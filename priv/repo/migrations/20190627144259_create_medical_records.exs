defmodule DonateHappiness.Repo.Migrations.CreateMedicalRecords do
  use Ecto.Migration

  def change do
    create table(:medical_records) do
      add :blood_group, :string
      add :differently_abled, :boolean, default: false, null: false
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

  end
end
