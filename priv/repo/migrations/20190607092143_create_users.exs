defmodule DonateHappiness.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :fullname, :string
      add :email, :string
      add :hashed_password, :string
      add :reset_password_token, :string
      add :reset_password_sent_at, :utc_datetime
      add :dob, :date

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
