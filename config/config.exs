# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :donate_happiness,
  ecto_repos: [DonateHappiness.Repo]

# Configures the endpoint
config :donate_happiness, DonateHappinessWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "d9/WXRiRaF+8reF14rI2yV7QMU8saauk21mOZ2+Zn242I+V4e3+Gxs1vFd+TA0Pn",
  render_errors: [view: DonateHappinessWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: DonateHappiness.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Guardian config
config :donate_happiness, DonateHappiness.Guardian,
  issuer: "donate_happiness",
  secret_key: "2GS/vbLlLSsKvU9NsabMOGr43+OGarNbbV/OjMcSpzlBQucgXmuYnojgGmMyEOHm"
