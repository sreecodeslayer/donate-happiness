use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :donate_happiness, DonateHappinessWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :donate_happiness, DonateHappiness.Repo,
  username: "postgres",
  password: "postgres",
  database: "donate_happiness_test",
  hostname: "postgres",
  password: "postgress",
  pool: Ecto.Adapters.SQL.Sandbox
